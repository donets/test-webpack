const path = require('path');
const merge = require('webpack-merge');
const commonConfig = require('./webpack.config.common');
const webpack = require('webpack');

module.exports = merge(commonConfig, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'www'),
    writeToDisk: true, //принудительная запись файлов в dist
  },
  plugins: [
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false),
      'process.env': {NODE_ENV: JSON.stringify(process.env.NODE_ENV)},
      }),
  ]

});