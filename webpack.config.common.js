const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FileLoader = require('file-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const fs = require('fs')

function generateHtmlPlugins(templateDir) {
  const tmpFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
  const templateFiles = tmpFiles.filter(function(file) {
    return path.extname(file).toLowerCase() === '.html';
  });

  return templateFiles.map(item => {
    const parts = item.split('.');
    const name = parts[0];
    const extension = parts[1];
    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      favicon: './src/favicon/favicon.ico',
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
//      inject: false,
    })
  })
}

 const htmlPlugins = generateHtmlPlugins('./src')

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'js/bundle.[contenthash].js',
    path: path.resolve(__dirname, 'www'),
  },
  plugins: [
      new MiniCssExtractPlugin({
        filename: 'css/bundle.[contenthash].css',
      }),
      new CleanWebpackPlugin(),
      new CopyWebpackPlugin( [{ from: path.resolve(__dirname, 'src/data/'), to: path.resolve(__dirname, 'www/data/') }]),
  ].concat(htmlPlugins),

  module: {
     rules: [
       {
         test: /\.(sa|sc|c)ss$/,
         use: [
         {
           loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: ('../'),
            },
          },
          'css-loader',
          'sass-loader',
         ],
       },
       {
         test: /\.(woff|woff2|eot|ttf|otf)$/,
         use: [
          {
            loader: 'file-loader',
            options:{
             outputPath: 'fonts'
            }           
          }
         ],
       },

       {
         test: /\.(png|jpe?g|gif)$/i,
         exclude: path.resolve(__dirname, 'src/images'),
         use: [
         {
           loader: 'url-loader',
           options:{
            limit: 8192,
            outputPath: 'images'

           }
//           loader: 'file-loader',
//            options: {
//              outputPath: 'img'
//            },
          },
         ],
       },

       {
         test: /\.(png|jpe?g|gif)$/i,
         include: path.resolve(__dirname, 'src/images'),
         use: [
         {
           loader: 'file-loader',
           options:{
//            outputPath: 'images'
            name: '/images/[name].[ext]'
           }
          },
         ],
       },
       {
         test: /\.html$/,
         use: [
//          'file-loader?name=[name].[ext]', 
//          'extract-loader',

//            'raw-loader'
          {
            loader: 'html-loader',
            options: {
            },
          },
  
         ],
       },

     ],
   },
   
};